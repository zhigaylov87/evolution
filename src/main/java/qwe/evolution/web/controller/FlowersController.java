package qwe.evolution.web.controller;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import qwe.evolution.core.domain.flower.Color;
import qwe.evolution.core.domain.flower.Flower;
import qwe.evolution.core.domain.flower.Petal;
import qwe.evolution.core.domain.flower.Stem;
import qwe.evolution.core.mutation.flower.FlowerMutationStrategy;

import java.util.List;

@RestController
public class FlowersController {

    @Autowired
    private FlowerMutationStrategy mutationStrategy;

    @GetMapping("/flowers/{id}")
    public Flower get(@PathVariable Integer id) {
        return getFlower(id);
    }

    @GetMapping("/flowers/{id}/evolution")
    public List<List<Flower>> evolution(@PathVariable Integer id,
                                  @RequestParam Integer generationsCount,
                                  @RequestParam Integer repeatCount) {

        Flower initFlower = getFlower(id);
        List<List<Flower>> repeats = Lists.newArrayList();

        for (int i = 0; i < repeatCount; i++) {

            Flower flower = initFlower;
            List<Flower> generations = Lists.newArrayList(flower);

            for (int j = 0; j < generationsCount; j++) {
                flower = mutationStrategy.mutate(flower);
                generations.add(flower);
            }

            repeats.add(generations);
        }

        return repeats;
    }

    private Flower getFlower(Integer id) {
        return new Flower(
                new Stem(100, 5),
                ImmutableList.of(
                        new Petal(Color.RED, 100, 50, 17),
                        new Petal(Color.BLUE, 70, 20, 13),
                        new Petal(Color.PINK, 30, 10, 7)
                )
        );
    }
}
