package qwe.evolution.core.domain.flower;

import lombok.Value;

@Value
public class Stem {

    int height;
    int width;

}
