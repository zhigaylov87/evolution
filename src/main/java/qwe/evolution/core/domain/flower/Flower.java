package qwe.evolution.core.domain.flower;

import lombok.Value;
import qwe.evolution.core.domain.Organism;

import java.util.List;

@Value
public class Flower implements Organism {

    Stem stem;
    List<Petal> petals;

}
