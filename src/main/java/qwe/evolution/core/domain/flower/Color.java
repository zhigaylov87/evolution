package qwe.evolution.core.domain.flower;

import lombok.Value;

@Value
public class Color {

    public final static Color WHITE   = new Color(255, 255, 255);
    public final static Color RED     = new Color(255, 0, 0);
    public final static Color PINK    = new Color(255, 175, 175);
    public final static Color ORANGE  = new Color(255, 200, 0);
    public final static Color YELLOW  = new Color(255, 255, 0);
    public final static Color GREEN   = new Color(0, 255, 0);
    public final static Color MAGENTA = new Color(255, 0, 255);
    public final static Color CYAN    = new Color(0, 255, 255);
    public final static Color BLUE    = new Color(0, 0, 255);

    int red;
    int green;
    int blue;

}
