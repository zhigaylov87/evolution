package qwe.evolution.core.domain.flower;

import lombok.Value;

@Value
public class Petal {

    Color color;
    int length;
    int peakLength;
    int count;

}
