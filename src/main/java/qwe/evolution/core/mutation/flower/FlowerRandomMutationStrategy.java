package qwe.evolution.core.mutation.flower;

import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Component;
import qwe.evolution.core.domain.flower.Color;
import qwe.evolution.core.domain.flower.Flower;
import qwe.evolution.core.domain.flower.Petal;
import qwe.evolution.core.domain.flower.Stem;
import qwe.evolution.core.mutation.RandomMutationStrategy;

import java.util.ArrayList;
import java.util.List;

@Component
public class FlowerRandomMutationStrategy implements FlowerMutationStrategy, RandomMutationStrategy<Flower> {

    private static final int SIZE_STEP = 10;

    private static final int RGB_STEP = 50;
    private static final int RGB_LIMIT = 255;

    private static final int COUNT_STEP = 10;


    private enum MutationDirect {

        STEM_HEIGHT,
        STEM_WIDTH,
        PETAL_COLOR,
        PETAL_LENGTH,
        PETAL_PEAK_LENGTH,
        PETAL_COUNT

    }

    @Override
    public Flower mutate(Flower parent) {

        Stem stem = parent.getStem();

        List<Petal> petals = parent.getPetals();
        int petalIdx = randomInt(petals.size());
        Petal petal = petals.get(petalIdx);

        MutationDirect mutationDirect = randomElement(MutationDirect.values());
        System.out.println("mutationDirect = " + mutationDirect);
        switch (mutationDirect) {

            case STEM_HEIGHT:
                stem = new Stem(randomChange(stem.getHeight(), SIZE_STEP), stem.getWidth());
                break;

            case STEM_WIDTH:
                stem = new Stem(stem.getHeight(), randomChange(stem.getWidth(), SIZE_STEP));
                break;

            case PETAL_COLOR:
                petal = new Petal(
                        mutate(petal.getColor()),
                        petal.getLength(),
                        petal.getPeakLength(),
                        petal.getCount()
                );
                break;

            case PETAL_LENGTH:
                petal = new Petal(
                        petal.getColor(),
                        randomChange(petal.getLength(), SIZE_STEP),
                        petal.getPeakLength(),
                        petal.getCount()
                );
                break;

            case PETAL_PEAK_LENGTH:
                petal = new Petal(
                        petal.getColor(),
                        petal.getLength(),
                        randomChange(petal.getPeakLength(), SIZE_STEP, (int) (0.9 * petal.getLength())),
                        petal.getCount()
                );
                break;

            case PETAL_COUNT:
                petal = new Petal(
                        petal.getColor(),
                        petal.getLength(),
                        petal.getPeakLength(),
                        randomChange(petal.getCount(), COUNT_STEP)
                );
                break;

            default:
                throw new RuntimeException("Unsupported mutation direct: " + mutationDirect);
        }

        petals = new ArrayList<>(petals);
        petals.set(petalIdx, petal);

        return new Flower(stem, ImmutableList.copyOf(petals));
    }

    private Color mutate(Color parent) {
        return new Color(
                randomChange(parent.getRed(), RGB_STEP, RGB_LIMIT),
                randomChange(parent.getGreen(), RGB_STEP, RGB_LIMIT),
                randomChange(parent.getBlue(), RGB_STEP, RGB_LIMIT)
        );
    }
}
