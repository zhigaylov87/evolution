package qwe.evolution.core.mutation.flower;

import qwe.evolution.core.domain.flower.Flower;
import qwe.evolution.core.mutation.MutationStrategy;

public interface FlowerMutationStrategy extends MutationStrategy<Flower> {
}
