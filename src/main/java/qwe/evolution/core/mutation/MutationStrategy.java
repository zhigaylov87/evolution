package qwe.evolution.core.mutation;

import qwe.evolution.core.domain.Organism;

public interface MutationStrategy<T extends Organism> {

    T mutate(T parent);

}
