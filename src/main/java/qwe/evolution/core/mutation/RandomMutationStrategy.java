package qwe.evolution.core.mutation;

import qwe.evolution.core.domain.Organism;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

public interface RandomMutationStrategy<T extends Organism> extends MutationStrategy<T> {

    default int randomInt(int limitExclusive) {
        return new Random().nextInt(limitExclusive);
    }

    default <E> E randomElement(Collection<E> elements) {
        return new ArrayList<E>(elements).get(randomInt(elements.size()));
    }

    default <E> E randomElement(E[] elements) {
        return Arrays.asList(elements).get(randomInt(elements.length));
    }

    default int randomChange(int base, int step) {
        int value = new Random().nextBoolean() ? base + step : base - step;
        return Integer.max(value, step);
    }

    default int randomChange(int base, int step, int limit) {
        return Integer.min(randomChange(base, step), limit);
    }

}
