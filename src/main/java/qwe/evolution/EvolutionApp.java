package qwe.evolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvolutionApp {

    public static void main(String[] args) {
        SpringApplication.run(EvolutionApp.class, args);
    }

}
