$(function() {

    $("#run-flowers-evolution").click(function() {
        runFlowersEvolution();
    });


    function runFlowersEvolution() {
        console.log(">>> runFlowersEvolution >>> " + new Date());

        // showFlower(1);
        showFlowerEvolution(1, 100, 3);
    }

    function renderStar(canvasId, color, x, y, r, p, m) {
        var canvas = document.getElementById(canvasId);
        var ctx = canvas.getContext("2d");
        ctx.save();
        ctx.fillStyle = 'rgb(' + color.red + ',' + color.green + ',' + color.blue + ')';
        ctx.beginPath();
        ctx.translate(x, y);
        ctx.moveTo(0, 0 - r);
        for (var i = 0; i < p; i++) {
            ctx.rotate(Math.PI / p);
            ctx.lineTo(0, 0 - (r*m));
            ctx.rotate(Math.PI / p);
            ctx.lineTo(0, 0 - r);
        }
        ctx.fill();
        ctx.restore();
    }
    
    function renderFlower(canvas, flower) {
        flower.petals.forEach(function(petal) {
            renderStar(canvas, petal.color, 150, 150, petal.length, petal.count, petal.peakLength / petal.length);
        });
    }

    function renderFlowers(repeats, generationsCount, repeatCount) {
        var tbody = $("#flowers-canvas-table tbody");
        tbody.empty();

        var canvasIdToFlower = {};

        for (var generation = 0; generation < generationsCount; generation++) {

            var tr = $("<tr/>");
            for (var repeat = 0; repeat < repeatCount; repeat++) {

                var flower = repeats[repeat][generation];

                var canvasId = "flower-canvas-" + repeat + "-" + generation;
                var canvas = $('<canvas id="' + canvasId + '" width="300" height="300"/>');

                tr.append($("<td/>").append(canvas));

                canvasIdToFlower[canvasId] = flower;
            }
            tbody.append(tr);
        }

        for (var canvasId in canvasIdToFlower) {
            renderFlower(canvasId, canvasIdToFlower[canvasId]);
        }
    }

    function showFlower(id) {
        $.ajax({
            url: "/flowers/" + id,
            type: "GET"
        }).then(function(flower) {
            renderFlower(flower);
        }, function (jqXHR) {
            console.log(jqXHR);
        });
    }

    function showFlowerEvolution(id, generationsCount, repeatCount) {
        $.ajax({
            url: "/flowers/" + id + "/evolution?generationsCount=" + generationsCount + "&repeatCount=" + repeatCount,
            type: "GET"
        }).then(function(repeats) {
            renderFlowers(repeats, generationsCount, repeatCount);
        }, function (jqXHR) {
            console.log(jqXHR);
        });
    }






});